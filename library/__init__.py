"""
PyCat APNS.  Interface with Apple Push Notification service.
"""
__author__ = 'Gerardo Ezquerra'
__author_email__ = 'catwasandroid@gmail.com'
__license__ = 'BSD'
__url__ = 'https://bitbucket.org/catwashere/pycat-apns'
__version__ = '0.0.0.dev9'
