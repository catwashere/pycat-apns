"""
Setup for PyCat APNS
"""
from  __future__ import print_function

from glob import glob
from os import remove
from os.path import abspath, dirname, join
from shlex import split
from shutil import rmtree
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

import library as package


class Tox(TestCommand):
    """Integration of tox via the setuptools ``test`` command"""
    # pylint: disable=attribute-defined-outside-init
    user_options = [('tox-args=', 'a', "Arguments to pass to tox")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.tox_args = None

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        from tox import cmdline  # pylint: disable=import-error
        args = self.tox_args
        if args:
            args = split(self.tox_args)
        cmdline(args=args)


class Clean(TestCommand):
    """A setuptools ``clean`` command. Removes build files and folders"""

    def run(self):
        delete_in_root = [
            'build',
            'dist',
            '.eggs',
            '*.egg-info',
            '.tox',
        ]
        delete_everywhere = [
            '__pycache__',
            '*.pyc',
        ]
        for candidate in delete_in_root:
            rmtree_glob(candidate)
        for visible_dir in glob('[A-Za-z0-9]*'):
            for candidate in delete_everywhere:
                rmtree_glob(join(visible_dir, candidate))
                rmtree_glob(join(visible_dir, '*', candidate))
                rmtree_glob(join(visible_dir, '*', '*', candidate))


def rmtree_glob(file_glob):
    """Platform independent rmtree. Removes a complete directory."""
    for fobj in glob(file_glob):
        try:
            rmtree(fobj)
            print('%s/ removed ...' % fobj)
        except OSError:
            try:
                remove(fobj)
                print('%s removed ...' % fobj)
            except PermissionError as err:
                print('Permission denied.  %s' % fobj)
                exit('Try running the command as superuser (root, sudo).')
            except OSError as err:
                print(err)


def read_file(filename):
    """Read the contents of a file located relative to setup.py"""
    with open(join(abspath(dirname(__file__)), filename)) as thefile:
        return thefile.read()


setup(
    name='pycat-apns',
    version=package.__version__,
    description=package.__doc__.strip(),
    long_description=read_file('README.rst'),
    license=package.__license__,
    author=package.__author__,
    author_email=package.__author_email__,
    url=package.__url__,
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Communications',
        'Topic :: Communications :: Internet Phone',
        'Topic :: Software Development',
    ],
    keywords=['mobile', 'apple', 'iOS', 'push-notifications'],
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='tests',
    tests_require=['tox'],
    cmdclass={
        'clean': Clean,
        'test': Tox,
    },
)
