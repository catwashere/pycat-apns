Introduction
============

A full set of open source Python classes (v2.6 - v3.x) to interact with the
Apple Push Notification service for the iPhone, iPad and the iPod Touch.
Classes are based on `ApnsPHP`_.

In the Apple Push Notification Binary protocol there isn't a real-time feedback
about the correctness of notifications pushed to the server. So, after each
write to the server, the Push class waits for the "read stream" to change its
status (or at least N microseconds); if it happened and the client socket
receives an "end-of-file" from the server, the notification pushed to the
server was broken, the Apple server has closed the connection and the client
needs to reconnect to send other notifications still on the message queue.

To speed-up the sending activities the Push Server class can be used to create
a Push Notification Server with many processes that reads a common message
queue and sends parallel Push Notifications.

All client-server activities are based on the "on error, retry" pattern with
customizable timeouts, retry times and retry intervals.


Certificate Creation
====================

To send Push notification to an application/device couple you need an unique
device token and a certificate.

Generate a Push Certificate
---------------------------

To generate a certificate on a Mac OS X:

- Log-in to the `iPhone Developer Program Portal`_
- Expand Identifiers and choose App IDs from the menu on the right (or
  `click here`_)
- Click on the desire App ID and click Edit
- If you don't have an an App ID create an App ID without a wildcard.
  For example prefix.com.example.test
- If you have push notifications disabled in App Services just click on
  checkbox to enable it
- Click on Create Certificate... to start the wizard to generate a new
  Development Push SSL Certificate
  (`Apple Documentation: Creating the SSL Certificate and Keys`_)
- Download the certificate and double click on ``aps_developer_identity.cer``
  to import it into your Keychain
- Launch Keychain Assistant (located in Application, Utilities or search for
  it with Spotlight) and click on *My Keys* on the left
- Expand desire private key
- Right-click on certificate and choose "Export" and save as ``apns-cert.p12``
  (don't type a password).
- Right-click on private key and choose "Export" and save as ``apns-key.p12``
  (don't type a password).
- Open Terminal and change directory to location used to save the p12 and
  convert the PKCS12 certificate bundle into PEM format using the following
  commands:

Commands:

.. code-block:: bash

    $ openssl pkcs12 -clcerts -nokeys -out apns-cert.pem -in apns-cert.p12
    $ openssl pkcs12 -nocerts -out apns-key.pem -in apns-key.p12
    $ openssl rsa -in apns-key.pem -out apns-key-noenc.pem
    $ cat apns-dev-cert.pem apns-key-noenc.pem > apns_certification.pem

Now you can use this PEM file as your certificate in APNS!

Verify peer using Entrust Root Certification Authority
------------------------------------------------------

Download the Entrust Root Authority certificate directly from Entrust Inc.
website:

- Navigate to `Entrust Downloads`_
- Choose "Personal Use"
- Download the Entrust CA (2048) file (`entrust_2048_ca.cer`_) for the Sandbox
  and Production environment.  Apple said:

    "To ensure you can continue to validate your server's connection to the
    Apple Push Notification service, you will need to update your push
    notification server with a copy of the 2048-bit root certificate from
    Entrust's website."

You can do a fast download opening terminal and executing:

.. code-block:: bash

    $ wget https://www.entrust.net/downloads/binary/entrust_2048_ca.cer -O - > entrust_root_certification_authority.pem

Otherwise (for use only in a Mac OS X environment), export the Entrust Root
Authority certificate:

- Launch Keychain Assistant (located in Application, Utilities or search for
  it with Spotlight) and click on System Root Certificate on top-left and
  Certificates on the bottom-left
- Right-click on Entrust Root Certification Authority and export with
  ``entrust_root_certification_authority.pem`` file name and choose as document
  format Privacy Enhanced Mail (.pem).
- Now you can use this PEM file as Entrust Root Certification Authority in APNS
  to verify Apple Peer!


How To Contribute
=================

Write and run tests:

.. code-block:: bash

    $ python setup.py test

This command will execute `tox`_ and run static code analysis (flake8, PyLint)
and unit tests (py.test).  All dependencies for test execution are installed
automatically.  Due to a shortcoming of tox you may still have to install
``virtualenv`` manually though beforehand:

.. code-block:: bash

    $ pip install virtualenv
    
Use the *clean* command to remove build files and folders from the project
every once in a while:

.. code-block:: bash

    $ python setup.py clean


.. _ApnsPHP: http://code.google.com/p/apns-php
.. _iPhone Developer Program Portal: https://developer.apple.com/account/ios/certificate/certificateList.action
.. _click here: https://developer.apple.com/account/ios/identifiers/bundle/bundleList.action
.. _Apple Documentation\: Creating the SSL Certificate and Keys:
    http://developer.apple.com/library/ios/#documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ProvisioningDevelopment.html
.. _Entrust Downloads: https://www.entrust.net/downloads/root_index.cfm
.. _entrust_2048_ca.cer: https://www.entrust.net/downloads/binary/entrust_2048_ca.cer
.. _tox: https://tox.readthedocs.io/en/latest/
